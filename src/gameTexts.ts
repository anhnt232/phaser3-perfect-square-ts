export const GameTexts = {
    level: 'level',
    infoLines: ['tap and hold to grow', 'release to drop'],
    landHere: 'land here',
    success: 'Yeah!!!',
    failure: 'On no!!'
}