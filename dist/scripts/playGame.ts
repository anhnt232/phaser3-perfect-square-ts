import { GameOptions } from './gameOptions';
import { GameModes } from './gameModes';
import { GameTexts } from './gameTexts';
import GameWall from './gameWall';
import SquareText from './squareText';
import PlayerSquare from './playerSquare';

export class PlayGame extends Phaser.Scene {

    saveData: any;
    leftSquare: GameWall;
    rightSquare: GameWall;
    leftWall: GameWall;
    rightWall: GameWall;
    square: PlayerSquare;
    squareText: SquareText;
    infoGroup: Phaser.GameObjects.Group;
    levelText: Phaser.GameObjects.BitmapText;
    squareTweenTargets:any[];
    currentGameMode: number = GameModes.IDLE;
    gameWidth: number;
    gameHeight: number;
    growTween: Phaser.Tweens.Tween;
    rotateTween: Phaser.Tweens.Tween;

    constructor() {
        super({
            key: 'PlayGame'
        });
    }
  
    create(): void {
        this.gameWidth = this.game.config.width as number;
        this.gameHeight = this.game.config.height as number;
        this.saveData = localStorage.getItem(GameOptions.localStorageName) == null ? { level: 1} : JSON.parse(localStorage.getItem(GameOptions.localStorageName) as string);
        let tintColor = Phaser.Utils.Array.GetRandom(GameOptions.bgColors);
        this.cameras.main.setBackgroundColor(tintColor);
        this.placeWalls();
        this.square = new PlayerSquare(this, this.gameWidth / 2, -400, 'square');
		this.add.existing(this.square);
        this.squareText = new SquareText(this, this.square.x, this.square.y, 'font', this.saveData.level, 120, tintColor);
		this.add.existing(this.squareText);
        this.squareTweenTargets = [this.square, this.squareText];
        this.levelText = this.add.bitmapText(this.gameWidth / 2, 0, 'font', 'level ' + this.saveData.level, 60);
        this.levelText.setOrigin(0.5, 0);
        this.updateLevel();
        this.input.on('pointerdown', this.grow, this);
        this.input.on('pointerup', this.stop, this);
    }

    placeWalls(): void {
        this.leftSquare = new GameWall(this, 0, this.gameHeight, 'base', new Phaser.Math.Vector2(1, 1));
        this.add.existing(this.leftSquare);
        this.rightSquare = new GameWall(this, this.gameWidth, this.gameHeight, 'base', new Phaser.Math.Vector2(0, 1));
        this.add.existing(this.rightSquare);
        this.leftWall = new GameWall(this, 0, this.gameHeight - this.leftSquare.height, 'top', new Phaser.Math.Vector2(1, 1));
        this.add.existing(this.leftWall);
        this.rightWall = new GameWall(this, this.gameWidth, this.gameHeight - this.leftSquare.height, 'top', new Phaser.Math.Vector2(0, 1));
        this.add.existing(this.rightWall);
    }

    updateLevel(): void {
        let holeWidth = Phaser.Math.Between(GameOptions.holeWidthRange[0], GameOptions.holeWidthRange[1]);
        let wallWidth = Phaser.Math.Between(GameOptions.wallRange[0], GameOptions.wallRange[1]);
        this.leftSquare.tweenTo((this.gameWidth - holeWidth) / 2);
        this.rightSquare.tweenTo((this.gameWidth + holeWidth) / 2);
        this.leftWall.tweenTo((this.gameWidth - holeWidth) / 2 - wallWidth);
        this.rightWall.tweenTo((this.gameWidth + holeWidth) / 2 + wallWidth);
        this.tweens.add({
            targets: this.squareTweenTargets,
            y: 150,
            scaleX: 0.2,
            scaleY: 0.2,
            angle: 50,
            duration: 500,
            ease: 'Cubic.easeOut',
            callbackScope: this,
            onComplete: function() {
                this.rotateTween = this.tweens.add({
                    targets: this.squareTweenTargets,
                    angle: 40,
                    duration: 300,
                    yoyo: true,
                    repeat: -1
                });
                if (this.square.successful == 0) {
                    this.addInfo(holeWidth, wallWidth);
                }
                this.currentGameMode = GameModes.WAITING;
            }
        })
    }

    grow(): void {
        if (this.currentGameMode == GameModes.WAITING) {
            this.currentGameMode = GameModes.GROWING;
            if (this.square.successful == 0) {
                this.infoGroup.toggleVisible();
            }
            this.growTween = this.tweens.add({
                targets: this.squareTweenTargets,
                scaleX: 1,
                scaleY: 1,
                duration: GameOptions.growTime
            });
        }
    }

    stop(): void {
        if (this.currentGameMode == GameModes.GROWING) {
            this.currentGameMode = GameModes.IDLE;
            this.growTween.stop();
            this.rotateTween.stop();
            this.rotateTween = this.tweens.add({
                targets: this.squareTweenTargets,
                angle: 0,
                duration:300,
                ease: 'Cubic.easeOut',
                callbackScope: this,
                onComplete: function(){
                    if (this.square.displayWidth <= this.rightSquare.x - this.leftSquare.x) {
                        this.tweens.add({
                            targets: this.squareTweenTargets,
                            y: this.gameHeight + this.square.displayWidth,
                            duration:600,
                            ease: 'Cubic.easeIn',
                            callbackScope: this,
                            onComplete: function(){
                                this.levelText.text = 'Oh no!!!';
                                this.gameOver();
                            }
                        })
                    }
                    else{
                        if (this.square.displayWidth <= this.rightWall.x - this.leftWall.x) {
                            this.fallAndBounce(true);
                        }
                        else{
                            this.fallAndBounce(false);
                        }
                    }
                }
            })
        }
    }

    fallAndBounce(success: Boolean): void {
        let destY = this.gameHeight - this.leftSquare.displayHeight - this.square.displayHeight / 2;
        let message = success ? GameTexts.success : GameTexts.failure;
        if (success) {
            this.square.successful ++;
        }
        else {
            destY = this.gameHeight - this.leftSquare.displayHeight - this.leftWall.displayHeight - this.square.displayHeight / 2;
        }
        this.tweens.add({
            targets: this.squareTweenTargets,
            y: destY,
            duration:600,
            ease: 'Bounce.easeOut',
            callbackScope: this,
            onComplete: function() {
                this.levelText.text = message;
                if (!success) {
                    this.gameOver();
                }
                else{
                    this.time.addEvent({
                        delay: 1000,
                        callback: function() {
                            if (this.square.successful == this.saveData.level) {
                                this.saveData.level ++;
                                localStorage.setItem(GameOptions.localStorageName, JSON.stringify({
                                    level: this.saveData.level
                                }));
                                this.scene.start('PlayGame');
                            }
                            else {
                                this.squareText.updateText(this.saveData.level - this.square.successful);
                                this.levelText.text = 'level ' + this.saveData.level;
                                this.updateLevel();
                            }
                        },
                        callbackScope: this
                    });
                }
            }
        })
    }

    addInfo(holeWidth: number, wallWidth: number): void {
        this.infoGroup = this.add.group();
        let targetSquare = this.add.sprite(this.gameWidth / 2, this.gameHeight - this.leftSquare.displayHeight, 'square');
        targetSquare.displayWidth = holeWidth + wallWidth;
        targetSquare.displayHeight = holeWidth + wallWidth;
        targetSquare.alpha = 0.3;
        targetSquare.setOrigin(0.5, 1);
        this.infoGroup.add(targetSquare);
        let targetText = this.add.bitmapText(this.gameWidth / 2, targetSquare.y - targetSquare.displayHeight - 20, 'font', GameTexts.landHere, 48);
        targetText.setOrigin(0.5, 1);
        this.infoGroup.add(targetText);
        let holdText = this.add.bitmapText(this.gameWidth / 2, 250, 'font', GameTexts.infoLines[0], 40);
        holdText.setOrigin(0.5, 0);
        this.infoGroup.add(holdText);
        let releaseText = this.add.bitmapText(this.gameWidth / 2, 300, 'font', GameTexts.infoLines[1], 40);
        releaseText.setOrigin(0.5, 0);
        this.infoGroup.add(releaseText);
    }

    gameOver(): void {
        this.time.addEvent({
            delay: 1000,
            callback: function() {
                this.scene.start('PlayGame');
            },
            callbackScope: this
        });
    }
}