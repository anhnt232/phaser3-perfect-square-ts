import Phaser from 'phaser';
import { ScaleObject } from './scaleObject';
import { PreloadAssets } from './preloadAssets';
import { PlayGame} from './playGame';

const configObject: Phaser.Types.Core.GameConfig = {
    type: Phaser.AUTO,
    scale: ScaleObject,
    scene: [PreloadAssets, PlayGame]
}

new Phaser.Game(configObject);